const calcContainer = document.getElementById('calc-container');
let currentDeg = 0;

const gradient = [
    'hsl(240deg 87% 85%)',
    'hsl(234deg 79% 85%)',
    'hsl(228deg 75% 85%)',
    'hsl(221deg 67% 84%)',
    'hsl(215deg 63% 84%)',
    'hsl(209deg 56% 84%)',
    'hsl(203deg 49% 84%)',
    'hsl(197deg 43% 84%)',
    'hsl(192deg 36% 83%)',
    'hsl(185deg 30% 83%)',
  ];

function animateGradient() {
    // Update the current degree value (0 to 360)
    currentDeg = (currentDeg + 1) % 360;
  
    // Construct the linear-gradient string with dynamic angle
    const gradientString = `linear-gradient(${currentDeg}deg, ${gradient.join(', ')})`;
  
    // Set the background-image of the calc-container
    calcContainer.style.backgroundImage = gradientString;
  
    // Request animation frame for continuous animation
    requestAnimationFrame(animateGradient);
  }
animateGradient();