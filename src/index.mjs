import Tax from './tax.mjs';

const CONF = {
    INITIAL_PRICE: 1,
    TAX_RATE: 24,
}

const DOM = {
    /** @type {HTMLInputElement} */
    LIST_PRICE: document.getElementById('list-price'),
    /** @type {HTMLInputElement} */
    TAX_RATE: document.getElementById('tax-rate'),
    /** @type {HTMLInputElement} */
    TOTAL_PRICE: document.getElementById('total-price'),
    /** @type {HTMLInputElement} */
    DAY_NUMBER: document.getElementById('day-number'),
    /** @type {HTMLButtonElement} */
    INFLATION: document.getElementById('inflation'),
    /** @type {HTMLDivElement} */
    DAY: document.getElementById('day'),
    /** @type {HTMLDivElement} */
    CONT: document.getElementById('calc-container'),

}

const state = {
    /** @type {Array<number, number>} currently active fields */
    calc_based_on: ['list-price', 'tax-rate']
}

const changeValue = (field_name) => {
    if (!state.calc_based_on.includes(field_name)) {
        // active changed
        state.calc_based_on.unshift(field_name);
        state.calc_based_on.pop();
    } // else { // no change }
    if (state.calc_based_on.includes('list-price') && state.calc_based_on.includes('tax-rate')) {
        const list_price = parseFloat(DOM.LIST_PRICE.value);
        const tax_rate = parseFloat(DOM.TAX_RATE.value);
        DOM.TOTAL_PRICE.value = Tax.applyTax(list_price, tax_rate).toString();
    } else if (state.calc_based_on.includes('tax-rate') && state.calc_based_on.includes('total-price')) {
        const tax_rate = parseFloat(DOM.TAX_RATE.value);
        const total_price = parseFloat(DOM.TOTAL_PRICE.value);
        DOM.LIST_PRICE.value = Tax.deductTax(total_price, tax_rate).toString();
    } else { // includes 'total-price' && 'list-price'
        const total_price = parseFloat(DOM.TOTAL_PRICE.value);
        const list_price = parseFloat(DOM.LIST_PRICE.value);
        DOM.TAX_RATE.value = Tax.figureTaxRate(list_price, total_price);
    }
}
const setup = () => {
    DOM.LIST_PRICE.value = CONF.INITIAL_PRICE;
    DOM.TAX_RATE.value = CONF.TAX_RATE;
    DOM.LIST_PRICE.onchange = () => changeValue('list-price');
    DOM.TAX_RATE.onchange = () => changeValue('tax-rate');
    DOM.TAX_RATE.onchange();
    DOM.TOTAL_PRICE.onchange = () => changeValue('total-price');
    DOM.INFLATION.addEventListener("click", myScript)
}
function myScript() {
    console.log("Hello")
    DOM.DAY.style.visibility = "visible"
    let i = parseFloat(DOM.TOTAL_PRICE.value);
    applyAnimation()
    applyInflation(i)
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function applyAnimation() {
    let animationDuration = 4; 
  
    for (let i = 0; i < 5000; i++) {
      if (i > 5 && i <= 10) {
        animationDuration = 1;  // Short animation for i between 6-10
      } else if (i > 10 && i <= 15) {
        animationDuration = 4;  // Back to default duration
      } else if (i > 15 && i <= 20) {
        animationDuration = 1;  // Short animation for i between 16-25
      } else if (i > 20 && i <= 25) {
        animationDuration = 0.4; // Faster animation for i between 26-50
      } else if (i > 25) {
        animationDuration = 0.1; // Fastest animation for i above 50
      }
  
      DOM.CONT.style.animation = `crazyAnim${i > 50 ? 2 : 1 || 2} ${animationDuration}s linear 0s 10000 normal forwards`;
      await sleep(800);
    }
  }

async function applyInflation(i) {
    let increment_speed = 3000;
    let increase = 2;
    let start = i
    let modifier = 120
    i = 0
    while (i < 2000000) {
        let curr_increment_speed = increment_speed - modifier*i;
        let curr_inc = increase + i * i
        start += curr_inc
        i++;
        DOM.TOTAL_PRICE.value=start;
        DOM.DAY_NUMBER.value=2023+i
        console.log("i", i)
        console.log("VALUE", start);
        console.log("SLEEP", curr_increment_speed)
        await sleep(curr_increment_speed);
      }
}

setup();
